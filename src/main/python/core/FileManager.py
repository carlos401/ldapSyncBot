"""

ldapSyncBot
Copyright (C) 2018  CrabTech

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import datetime

class FileManager:
    
    def write(self, fileName, data):
        name = "../logs/" + fileName
        self.file = open(name, "a")
        self.file.write(str(datetime.datetime.now()))
        self.file.write("\t" + data + "\n\n")
        self.file.close()
    
    def overwriteFile(self, fileName):
        name = "../logs/" + fileName
        self.file = open(name, "w")
        self.file.write("")
        self.file.close()
    
    def seeFileContent(self, fileName):
        name = "../logs/" + fileName
        self.file = open(name, "r")
        print(self.file.read())
        self.file.close()