"""

ldapSyncBot
Copyright (C) 2018  CrabTech

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import time  # used to take a loop
import telepot  # import telepot framework
from ldap3.core.exceptions import LDAPSocketOpenError, LDAPBindError  # the LDAP implementation
import threading  # to manage multithreading

from src.main.python.core.FileManager import FileManager
from src.main.python.core.OpenLDAP import OpenLDAP


class XBOT(threading.Thread):
    """
    Main class in project
    """

    __messages = {
        'config error': 'Lo siento, solo puedes configurarme en un chat privado',
        'about': 'ldapSyncBot\nCopyright (C) 2018  CrabTech\nThis program comes with ABSOLUTELY NO WARRANTY.\nThis is '
                 'free software, and you are welcome to redistribute it,\nunder certain conditions; check our repo '
                 'for details.',
        'unknown_group': 'El comando no puede ejecutarse en este grupo',
        'unknown_private': 'El comando no ha podido interpretarse, contacte a los desarrolladores'
    }

    __feedback = []

    def __init__(self, io_manager, bot):
        """
        Constructor
        """
        threading.Thread.__init__(self)  # the thread itself
        self.__bot = bot  # the real connection to telegram channel
        self.__groups_called = {}  # groups that has called the Bot
        self.__idents = {}  # who are in which group?
        self.__ldap = None
        self.__file_man = FileManager()

    def run(self):
        """
        For threading
        """
        while 1:
            time.sleep(10)

    def handle_msg(self, msg):
        """
        Allows manage conversations
        :param msg: the message
        :return:
        """

        flavor = telepot.flavor(msg)  # index using flavor

        if flavor == 'chat':

            content_type, chat_type, chat_id, date, message_id = telepot.glance(msg, 'chat',
                                                                                True)  # extracts all data from msg
            if content_type == 'text':

                if msg['text'] == '/about' or msg['text'] == '/about@LDAPSyncBot':  # prints info about Bot
                    self.send_message(chat_id, self.__messages['about'])

                elif msg['text'] == '/feedback' or msg['text'] == '/feedback@LDAPSyncBot':
                    self.send_message(chat_id, 'Responde a este mensaje con tus comentarios')

                elif chat_type == 'group':  # case group
                    if 'new_chat_participant' in msg:
                        pass

                    elif msg['text'] == '/bye@LDAPSyncBot':
                        self.__bot.leaveChat(chat_id)

                    elif msg['text'] == '/meet@LDAPSyncBot':
                        self.__idents[(msg['from']['first_name'])] = (msg['from']['id'])

                    elif msg['text'] == '/update@LDAPSyncBot':
                        self.update_groups()
                        pass

                    elif 'reply_to_message' in msg:
                        if msg['reply_to_message']['text'] == 'Responde a este mensaje con tus comentarios':
                            self.__file_man.write("feedback.txt", msg['text'])
                            self.send_message(chat_id,
                                              'Apreciamos tus comentarios \nLa mejora continua se traduce en un mejor '
                                              'servicio para ti')

                    else:
                        self.send_message(chat_id, self.__messages['unknown_group'])

                elif chat_type == 'private':  # case private

                    if 'reply_to_message' in msg:
                        self.private_message_replies_handler(chat_id, msg)  # reply private messages

                    elif msg['text'] == '/server' or msg['text'] == '/server@LDAPSyncBot':
                        if self.__ldap is not None:
                            self.send_message(chat_id, 'Tengo bajo mi control a ' + self.__ldap.get_host())
                        else:
                            self.send_message(chat_id, 'Vaya! Parece que no hay ningún server bajo mi control')
                            self.__last_command = ''

                    elif msg['text'] == '/config' or msg['text'] == '/config@LDAPSyncBot':
                        self.send_message(chat_id, 'Introduzca un nuevo dominio del servidor')

                    else:
                        self.send_message(chat_id, self.__messages['unknown_private'])

                elif chat_type == 'channel':
                    pass

            elif content_type == 'new_chat_member':
                self.__groups_called[(msg['chat']['title'])] = chat_id  # add this group to my list of groups

            elif content_type == 'left_chat_member':
                pass

            elif content_type == 'new_chat_title':
                pass

        elif flavor == 'callback_query':
            print('call')
            pass

        elif flavor == 'inline_query':
            pass

        elif flavor == 'chosen_inline_result':
            pass

        elif flavor == 'shipping_query':
            pass

        elif flavor == 'pre_checkout_query':
            pass

    def has_group(self, group_name):
        return group_name in self.__groups_called

    def send_message(self, chat_id, message):
        return self.__bot.sendMessage(chat_id, message)

    def send_contact(self, chat_id, name, number):
        return self.__bot.sendContact(chat_id=chat_id, phone_number=number, first_name=name)

    def connect_with_ldap(self, domain, user, passw):
        """
        Connects and saves the instance of LDAP
        :param domain: the host of server
        :param user: the username
        :param passw: the password
        :return: true if connection was successful
        """
        self.__ldap = OpenLDAP(domain, user, passw)
        return self.__ldap.connect_ldap()

    def update_groups(self):
        """
        Read groups in LDAP
        :return: None
        """
        try:
            ldap_groups = {}
            grp = self.__ldap.get_groups_ldap()  # read all groups in ldap
            for group in grp:
                usr = self.__ldap.get_users_ldap(str(group['o']))  # read all users in group
                for user in usr:
                    ldap_groups[str(group)] = str(user)  # append in dictionary
                    if str(user['cn']) not in self.__idents:
                        number = self.__ldap.get_phone_ldap(str(group['o']), str(user['cn']))[-1]
                        self.send_contact(chat_id=self.__groups_called[str(group['o'])], name=str(user['cn']),
                                          number="+506" + str(number['telephoneNumber']))

            for user in self.__idents:
                for group in ldap_groups:
                    if user not in ldap_groups[group]:
                        trash, group_name = group.split("o: ")
                        group_name = group_name.strip()
                        try:
                            self.__bot.kickChatMember(self.__groups_called[group_name], self.__idents.get(user))
                        except telepot.exception.TelegramError:
                            print("Imposible eliminar los usuarios en este grupo")
        except KeyError:
            print("No se reconoció el grupo solicitado")

        except Exception:
            print('Error en la actualización de grupos')  # for managing exceptions
            raise

    def private_message_replies_handler(self, chat_id, msg):
        if msg['reply_to_message']['text'] == 'Introduzca un nuevo dominio del servidor':
            self.__temp_host = msg['text']
            self.send_message(chat_id, 'Introduzca su nombre de usuario')

        elif msg['reply_to_message']['text'] == 'Introduzca su nombre de usuario' and self.__temp_host is not None:
            self.__temp_user = msg['text']
            self.send_message(chat_id, 'Introduzca su contraseña de acceso')

        elif msg['reply_to_message']['text'] == 'Introduzca su nombre de usuario' and self.__temp_host is None:
            self.send_message(chat_id, 'Primero defina el dominio del server')

        elif msg['reply_to_message']['text'] == 'Introduzca su contraseña de acceso' and self.__temp_user is not None:
            self.__temp_pass = msg['text']
            self.send_message(chat_id, 'Creando una conexión segura con ' + self.__temp_host + '\n(esto puede tardar '
                                                                                               'unos minutos)')
            try:
                if self.connect_with_ldap(self.__temp_host, self.__temp_user, self.__temp_pass):
                    self.send_message(chat_id, 'Conexión exitosa')
                    #  self.update_groups() ojo, activar ahorita
                    self.__temp_host = None
                else:
                    self.send_message(chat_id, 'Conexión fallida')
                    self.__temp_host = None
            except LDAPSocketOpenError:
                self.send_message(chat_id, 'No se pudo conectar al dominio especificado')
                self.__temp_host = None
            except LDAPBindError:
                self.send_message(chat_id, 'Vaya! Ha ocurrido un error de autenticación')
                self.__temp_user = None

        elif msg['reply_to_message']['text'] == 'Introduzca su contraseña de acceso' and self.__temp_user is None:
            self.send_message(chat_id, 'Primero defina el usuario por autenticar')

        elif msg['reply_to_message']['text'] == 'Responde a este mensaje con tus comentarios':
            self.__file_man.write("feedback.log",msg['text'])
            self.send_message(chat_id,
                              'Apreciamos tus comentarios \nLa mejora continua se traduce en un mejor servicio para ti')

    def print_idents(self):
        for index in self.__idents:
            print(index)
