"""

ldapSyncBot
Copyright (C) 2018  CrabTech

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

# Singleton patern based on the code licenced by Copyright (C) 2011 Radek Pazdera
# https://gist.github.com/pazdera/1098129


import time  # used to take a loop
import telepot  # import telepot framework
from telepot.loop import MessageLoop

from src.main.python.core.FileManager import FileManager
from src.main.python.core.XBOT import XBOT  # import from XBOT.py


class IOManager:
    """ This class is the receiver of messages, and it knows how to mannage each one
    It assigns a message to the corresponding XBOT who knows what has to do with it
    
    """
    # Here will be the instance stored.
    __instance = None

    # Here will be the access of Telegram API stored.
    __bot = None

    # A dictionary for management the XBOTs (threads) per user
    __xbots = {}

    __file_man = None

    @staticmethod
    def get_instance(token):
        """ Static access method. """
        if IOManager.__instance is None:
            IOManager(token)
        return IOManager.__instance

    def __init__(self, token):
        """ Virtually private constructor. """
        if IOManager.__instance is not None:
            raise Exception('This class is a singleton!')
        else:
            IOManager.__instance = self
            self.__bot = telepot.Bot(token)  # connect to Telegram API

    def __get_msgs(self):
        """ That methods starts listening messages. """
        MessageLoop(self.__bot, self.__handle_msg).run_as_thread()

    def __handle_msg(self, msg):
        """ This method manages the messages and assigns each one to the corresponding thread
        :param msg: the message received
        """
        #  CASE 1. new user has called the BOT
        if msg['chat']['type'] == 'private' and 'text' in msg and msg['text'] == '/start':
            if msg['from']['id'] not in self.__xbots:
                xbot = XBOT(self.get_instance, self.__bot)  # create new thread
                xbot.start()  # start
                self.__xbots[msg['from']['id']] = xbot  # append
            else:
                self.send_msgs(msg['chat']['id'], 'Al parecer ya nos conocemos...')  # el Bot ya había sido creado

        #  CASE 2. the BOT has been called in a group
        elif ('new_chat_participant' in msg) and (msg['new_chat_participant']['username'] == 'LDAPSyncBot'):
            thread = self.__xbots[msg['from']['id']]  # gets the corresponding thread
            thread.handle_msg(msg)  # assigns the msg to thread

        #  CASE 3. standard message
        else:
            # group message
            if msg['chat']['type'] == 'group':
                for xbt in self.__xbots.values():
                    if xbt.has_group(msg['chat']['title']):
                        xbt.handle_msg(msg)
                        break

            # individual message
            else:
                thread = self.__xbots[msg['from']['id']]
                thread.handle_msg(msg)

    def send_msgs(self, chat_id, msg):
        """
        Allows to send special messages for rejected commands
        :param chat_id:
        :param msg:
        :return:
        """
        self.__bot.sendMessage(chat_id, msg)

    def send_contact(self, chat_id, name, telephone):
        """

        :param chat_id:
        :param name:
        :param telephone:
        :return:
        """
        return self.__bot.sendContact(chat_id=chat_id, phone_number=telephone, first_name=name)

    def run(self):
        """ This method starts the execution.  """
        self.__get_msgs()  # start listening messages...
        while 1:
            time.sleep(10)
