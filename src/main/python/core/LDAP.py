"""

ldapSyncBot
Copyright (C) 2018  CrabTech

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

from interface import Interface


class LDAPInterface(Interface):
    """
    It is important to create interfaces for working
    """

    def get_host(self):
        """
        return the host
        :return:
        """
        pass

    def get_user(self):
        """
        return the user
        :return:
        """
        pass

    def connect_ldap(self):
        """
        allows connect to LDAP server
        :return:
        """
        pass

    def disconnect_ldap(self):
        """
        allows disconnect the LDAP server
        :return:
        """
        pass

    def get_users_ldap(self, group_name):
        """
        allows to search data into LDAP server
        :param group_name:
        :return:
        """
        pass

    def get_phone_ldap(self, group_name, user_name):
        """
        return all the telephone numbers for the users included in specific group_name
        :param user_name:
        :param group_name:
        :return:
        """
        pass

    def get_groups_ldap(self):
        """
        return all the groups into a base given
        :return:
        """
        pass
