"""

ldapSyncBot
Copyright (C) 2018  CrabTech

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

from interface import implements
from ldap3 import Server, Connection, LEVEL
from src.main.python.core.LDAP import LDAPInterface


class OpenLDAP(implements(LDAPInterface)):
    """
    This class is an implementation of LDAP interface
    """

    host = ""  # it is used to know the remote server
    port = ""  #

    user = ""  # for authentication
    __passw = ""  #

    server = None  # the object server
    conn = None  # allows manage connection

    def __init__(self, host, user, passw):
        """
        :param host: the host where is located the LDAP server
        :param user: user used to get access
        :param passw: for user authentication
        """
        self.host = host
        self.user = user
        self.__passw = passw
        self.server = Server(self.host)
        self.conn = Connection(self.server, user=self.user, password=self.__passw)

    def get_host(self):
        """
        returns the host using
        :return: the host of LDAP server
        """
        return self.host

    def get_user(self):
        """
        returns the username authenticated
        :return: the user authenticated in LDAP server
        """
        return self.user

    def connect_ldap(self):
        """
        allows connection with the remote LDAP server
        :return: true if connection was successful, false otherwise
        """
        return self.conn.bind()

    def disconnect_ldap(self):
        """
        allows disconnect the LDAP server
        :return: true if disconnection was successful, false otherwise
        """
        return self.conn.unbind()

    def __search_ldap(self, filt, attrib, base=None):
        """
        allows to send a query to LDAP server
        :param filt: a string that describes what you are searching for
        :param attrib: the list of attributes to get
        :param base: the location in the DIT where the search will start
        :return: the entries got from LDAP
        """
        if base is None:
            base = self.host
        self.conn.search(search_base=base, search_filter=filt, attributes=attrib, search_scope=LEVEL)
        return self.conn.entries

    def get_users_ldap(self, group_name):
        """
        return all the user names for the users included in specific group name
        :param group_name: the group to be used
        :return: a list of entries with users
        """
        return self.__search_ldap(base='o=' + group_name + ',dc=cloud,dc=hackinghouse,dc=pw',
                                  filt='(objectclass=person)', attrib=['cn'])

    def get_phone_ldap(self, group_name, user_name):
        """
        return all the telephone numbers for the users included in specific group name
        :param group_name: the group to be used
        :return: a list of entries with phone numbers
        """
        return self.__search_ldap(base='o=' + group_name + ',dc=cloud,dc=hackinghouse,dc=pw',
                                  filt='(cn='+user_name+')', attrib=['telephoneNumber'])

    def get_groups_ldap(self):
        """
        return all the groups into a base given
        :return: all groups in LDAP server
        """
        return self.__search_ldap(base='dc=cloud,dc=hackinghouse,dc=pw', filt='(objectclass=organization)',
                                  attrib=['o'])
