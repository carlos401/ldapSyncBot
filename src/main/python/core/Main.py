"""

ldapSyncBot
Copyright (C) 2018  CrabTech

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import sys
from src.main.python.core.IOManager import IOManager

if __name__ == '__main__':
    print(' ldapSyncBot',
          '\n Copyright (C) 2018  CrabTech.',
          '\n This program comes with ABSOLUTELY NO WARRANTY.'
          '\n This is free software, and you are welcome to redistribute it',
          '\n under certain conditions; see License file for details.')
    try:
        m = IOManager(sys.argv[1])
        m.run()
    except IndexError:
        print("\nError 001: El programa no puede iniciar sin un ID de telegram válido"
              "\nSolución: Proporcione un token de TelegramBot API por linea de comandos")
    except:
        print("\nError 002: Ocurrió un error inesperado"
              "\nSolución: Consulte la documentación o contacte a los desarrolladores")
