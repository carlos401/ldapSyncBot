"""

ldapSyncBot
Copyright (C) 2018  CrabTech

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

from unnittest import TestCase
import sys
import src


class IOManagerTest(TestCase):
    """ This class tests src/IOManager. """
    
    def setUp(self):
        self.IOManager = IOManager(sys.args[1])
    

class OpenLDAPTest(TestCase):
    """ This class tests src/OpenLDAP. """
    
    def setUp(self):
        self.OpenLDAP = OpenLDAP(sys.args[1])

class XBOTTest(TestCase):
    """ This class tests src/XBOT. """
    
    def setUp(self):
        self.XBOT = XBOT(sys.args[1])
    
    

