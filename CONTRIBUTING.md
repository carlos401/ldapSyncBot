In order to contributed to this repo, follow this steps:
1. Fork this project
2. Create a new branch from *master*
3. Developt and push your changes
4. Create a merge request

For more information, send a mail to: <carlos.delgadorojas@ucr.ac.cr>